package algorithms;

import java.util.ArrayList;
import java.util.List;

public class SubSet {
	
	static List<List<Integer>> ans = new ArrayList<>();
	
	public static void main(String[] args) {
		/*
		backtrack(new int[] {1,2,3} ,0, new ArrayList<>());
		ans.forEach(list->{
			System.out.print("[");
			list.forEach(System.out::print);
			System.out.print("]");
			System.out.println("");
		});
		*/
		System.out.println(1/10);
	}
	
	static void backtrack(int[] candidates ,int start, List<Integer> path) {
		ans.add(new ArrayList<>(path));
		for(int i = start ; i < candidates.length ; i++) {
			path.add(candidates[i]);
			backtrack(candidates,i+1,path);
			path.remove(path.size()-1);
		}
		
	}

}
