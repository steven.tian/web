package com.my.web.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.util.Strings;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.my.web.repository.BedSet;
import com.my.web.repository.BedSetDetail;
import com.my.web.repository.BedSetDetailRepository;
import com.my.web.repository.BedSetRepository;
import com.my.web.repository.Product;
import com.my.web.repository.ProductRepository;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(path = "/product") // This means URL's start with /demo (after Application path)
public class MainController {
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private BedSetRepository bedSetRepository;
	@Autowired
	private BedSetDetailRepository bedSetDetailRepository;
	ObjectMapper objectMapper = new ObjectMapper();

	@CrossOrigin
	@PostMapping(path = "/add")
	public @ResponseBody String addProduct(@RequestBody Product prod) {
		productRepository.deleteBynumber(prod.getNumber());
		productRepository.save(prod);
		return "{\"result\" : \"新增成功\"}";
	}
	
	@CrossOrigin
	@PostMapping(path = "/delete/{id}")
	public @ResponseBody String delProduct(@PathVariable String id) {
		productRepository.deleteBynumber(id);
		return "{\"result\" : \"刪除成功\"}";
	}

	@CrossOrigin
	@PostMapping(path = "/upload")
	public @ResponseBody String uploadProduct(HttpServletRequest req) {

		try {
			@SuppressWarnings("resource")
			Workbook workbook = WorkbookFactory.create(req.getInputStream());
			Sheet sheet = workbook.getSheetAt(0);

			BedSet bs = new BedSet();
			Row firstRow = sheet.getRow(0);
			Cell c = firstRow.getCell(0);
			String str = c.getRichStringCellValue().getString();
			int _idx = str.lastIndexOf(" ");
			bs.setNumber("*" + str.substring(0, _idx));
			bs.setName(str.substring(_idx + 1));
			
			BedSet copied = bedSetRepository.findByNumber(bs.getNumber());
			if(copied != null) {
				bs.setDescription(copied.getDescription());
				bs.setComment(copied.getComment());
			}
			
			bedSetRepository.deleteByNumber(bs.getNumber());
			
			bedSetRepository.save(bs);
			sheet.removeRow(firstRow);
			DecimalFormat formatter = new DecimalFormat("#,###");
			for (Row row : sheet) {
				BedSetDetail bsd = new BedSetDetail();
				for (Cell cell : row) {
					String content = null;
					switch (cell.getCellType()) {
					case STRING:
						content = cell.getRichStringCellValue().getString().trim();
						break;
					case NUMERIC:
						content = formatter.format((int) cell.getNumericCellValue());
						break;
					case BOOLEAN:
						break;
					case FORMULA:
						content = formatter.format((int) cell.getNumericCellValue());
						break;
					default:
						break;

					}

					int idx = cell.getColumnIndex();
					bsd.setNumber(bs.getNumber());
					switch (idx) {
					case 0:
						bsd.setCol1(content);
						break;
					case 1:
						bsd.setCol2(content);
						break;
					case 2:
						bsd.setCol3(content);
						break;
					case 3:
						bsd.setCol4(content);
						break;
					case 4:
						bsd.setCol5(content);
						break;
					case 5:
						bsd.setCol6(content);
						break;
					case 6:
						bsd.setCol7(content);
						break;
					case 7:
						bsd.setCol8(content);
						break;
					default:
						break;
					}

				}
				if (bsd.getNumber() != null) {
					this.bedSetDetailRepository.deleteByNumberAndCol1(bsd.getNumber(), bsd.getCol1());
					bedSetDetailRepository.save(bsd);
				}

			}
			
			return "{\"result\" : \"匯入成功\" , \"queryNumber\":\""+bs.getNumber()+"\"}";
		} catch (Exception e) {
			e.printStackTrace();
			return "{\"result\" : \"匯入失敗\"}";
		}
		
		
	}

	@CrossOrigin
	@PostMapping(path = "/bedSet")
	public @ResponseBody String addBedSet(@RequestBody String json) {
		try {
			JsonNode jsonNode = objectMapper.readTree(json);
			JsonNode partial = jsonNode.get("partial");
			String desc = jsonNode.get("description").asText("");
			BedSet bs = new BedSet();
			String number = partial.get("number").asText();
			delBedSet(number);
			bs.setName(partial.get("name").asText());
			bs.setNumber(number);
			bs.setComment(partial.get("comment").asText());
			bs.setDescription(desc);
			bedSetRepository.save(bs);
			JsonNode products = jsonNode.get("products");
			for (final JsonNode objNode : products) {
				BedSetDetail detail = objectMapper.treeToValue(objNode, BedSetDetail.class);
				detail.setNumber(number);
				bedSetDetailRepository.save(detail);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "{\"result\" : \"新增失敗\"}";
		}

		return "{\"result\" : \"新增成功\"}";
	}

	@CrossOrigin
	@DeleteMapping(path = "/bedSet/{id}")
	public @ResponseBody String deleteBedSet(@PathVariable String id) {
		try {
			delBedSet(id);
		} catch (Exception e) {
			e.printStackTrace();
			return "{\"result\" : \"刪除失敗\"}";
		}

		return "{\"result\" : \"刪除成功\"}";
	}

	
	
	private void delBedSet(String number) {
		bedSetDetailRepository.deleteByNumber(number);
		bedSetRepository.deleteByNumber(number);
	}

	@CrossOrigin
	@GetMapping(path = "/bedSet/{id}")
	public @ResponseBody ObjectNode getBedSet(@PathVariable String id) throws IOException {
		ObjectNode node = objectMapper.createObjectNode();
		BedSet bs = bedSetRepository.findByNumber(id);
		if (bs != null) {
			List<BedSetDetail> bsList = bedSetDetailRepository.findByNumber(id);
			JsonNode partial = objectMapper.convertValue(bs, JsonNode.class);
			ArrayNode products = objectMapper.convertValue(bsList, ArrayNode.class);
			node.set("partial", partial);
			node.set("products", products);
			node.put("description", bs.getDescription());
		}

		return node;
	}

	@CrossOrigin
	@GetMapping(path = "/check")
	public @ResponseBody String check(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String credential = request.getParameter("credential");
		if (credential.equals("1")) {
			return "{\"url\" : \"index\"}";
		} else if(credential.equals("admin")){
			return "{\"url\" : \"admin\" , \"token\":\"admin\" }";
		}else {
			return "{\"url\" : \"error.html\"}";
		}
	}

	@CrossOrigin
	@GetMapping(value = "/barcode/{id}")
	public @ResponseBody String getBarcodeInfo(@PathVariable String id) throws Exception {
		Product prod = productRepository.findBynumber(id);
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(prod);

	}

	@CrossOrigin
	@GetMapping(value = "/bedset/detail/{id}")
	public @ResponseBody ObjectNode getBedSetDetail(@PathVariable String id) throws Exception {
		ObjectNode node = objectMapper.createObjectNode();
		List<BedSetDetail> bsList = bedSetDetailRepository.findByNumber(id);
		ArrayNode products = objectMapper.convertValue(bsList, ArrayNode.class);
		node.set("products", products);
		return node;

	}

	@CrossOrigin
	@GetMapping(value = "/bedset/master/{id}")
	public @ResponseBody JsonNode getBedSetMaster(@PathVariable String id) throws Exception {
		BedSet bs = bedSetRepository.findByNumber(id);
		return objectMapper.convertValue(bs, JsonNode.class);
	}

	@CrossOrigin
	@GetMapping(value = "/bedset/desc/{id}")
	public @ResponseBody JsonNode getBedSetDesc(@PathVariable String id) throws Exception {
		BedSet bs = bedSetRepository.findByNumber(id);
		return objectMapper.convertValue(bs, JsonNode.class);
	}

	@CrossOrigin
	@GetMapping(value = "/genQRcode/{id}")
	public ResponseEntity<Resource> genQRcode(@PathVariable String id) throws Exception {
		BufferedImage image = MainController
				.generateQRCodeImage("http://barcode.eastasia.cloudapp.azure.com" + ":3000/login?id=" + id);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "jpg", baos);
		byte[] bytes = baos.toByteArray();
		ByteArrayResource resource = new ByteArrayResource(bytes);
		return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(resource);
	}
	
	@CrossOrigin
	@GetMapping(value = "/query/{number}")
	public @ResponseBody ArrayNode getItemByNumber(@PathVariable String number) throws Exception {
		ArrayNode node = JsonNodeFactory.instance.arrayNode();
		bedSetRepository.findByNumberContains(number).forEach(b -> {
			String num = b.getNumber();
			String name = b.getName();
			List<BedSetDetail> bsds = bedSetDetailRepository.findByNumber(num);
			String price5 = null;
			String price6 = null;
			if(bsds.size() > 0) {
				BedSetDetail bsd = bsds.get(bsds.size()-1);
				price5 = bsd.getCol5();
				price6 = bsd.getCol6();	
			}
			ObjectNode obj = JsonNodeFactory.instance.objectNode();
			obj.put("number", num);
			obj.put("name", name);
			obj.put("price", price5!=null?price5:price6);
			node.add(obj);
		});
		
		productRepository.findByNumberContains(number).forEach(b -> {
			String num = b.getNumber();
			String name = b.getName();
			String price = b.getPrice();
			ObjectNode obj = JsonNodeFactory.instance.objectNode();
			obj.put("number", num);
			obj.put("name", name);
			obj.put("price", price);
			node.add(obj);
		});
		
		return node;
	}

	public static BufferedImage generateQRCodeImage(String barcodeText) throws Exception {
		QRCodeWriter barcodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = barcodeWriter.encode(barcodeText, BarcodeFormat.QR_CODE, 200, 200);

		return MatrixToImageWriter.toBufferedImage(bitMatrix);
	}

}