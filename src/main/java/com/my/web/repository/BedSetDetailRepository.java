package com.my.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import jakarta.transaction.Transactional;

public interface BedSetDetailRepository extends JpaRepository<BedSetDetail, Long> {

	List<BedSetDetail> findByNumber(String number);
	
	@Transactional
	void deleteByNumberAndCol1(String number,String col1);
	
	@Transactional
	void deleteByNumber(String number);
	
}
