package com.my.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import jakarta.transaction.Transactional;

public interface ProductRepository extends JpaRepository<Product, Integer> {

	Product findBynumber(String number);
	List<Product> findByNumberContains(String number);
	
	@Transactional
	void deleteBynumber(String number);
}
