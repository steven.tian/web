package com.my.web.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import jakarta.transaction.Transactional;

public interface BedSetRepository extends JpaRepository<BedSet, Integer> {

	BedSet findByNumber(String number);
	
	List<BedSet> findByNumberContains(String number);
	
	@Transactional
	void deleteByNumber(String number);

}
